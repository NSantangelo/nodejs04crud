module.exports = (app) => {
    const notes = require('../controllers/note.controller.js');

    // crear nueva nota
    app.post('/notes', notes.create);

    // Recuperar todas las notas
    app.get('/notes', notes.findAll);

    // recuperar una nota desde un ID
    app.get('/notes/:noteId', notes.findOne);

    // Update a Note with noteId
    app.put('/notes/:noteId', notes.update);

    // Delete a Note with noteId
    app.delete('/notes/:noteId', notes.delete);
}