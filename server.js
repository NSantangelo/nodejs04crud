const express = require('express');
const bodyParser = require('body-parser');

// Configuring the database
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("conectado a la base de datos!");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// crear la app express
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded ???
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json ???
app.use(bodyParser.json())

// creacion api simple
app.get('/', (req, res) => {
    res.json({"message": "Bienvenido a la app EasyNotes. Tomá notas rapidamente. Organizá y guardá tus notas."});
});

// Require Notes routes
require('./app/routes/note.routes.js')(app);

// escuchando
app.listen(3000, () => {
    console.log("el servidor está escuchando en el puerto 3000");
});
